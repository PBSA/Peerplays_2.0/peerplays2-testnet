FROM registry.gitlab.com/pbsa/peerplays_2.0/discovery/master:latest

VOLUME /home/peerplays/peerplays-network/chains/

WORKDIR /home/peerplays/peerplays-network

ADD ./run.sh ./

CMD ["echo", "Base image dummy cmd"]
