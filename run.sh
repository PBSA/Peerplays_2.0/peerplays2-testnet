#!/bin/bash

BOOT_NODES=""
NAME=""
NODE_KEY=""
RPC_CORS=""
RPC_METHODS=""
STATE_PRUNING=""
SURI=""
VALIDATOR=""

while [ $# -gt 0 ] ; do
  case $1 in
    --bootnodes) BOOT_NODES="--bootnodes $2"
        echo "BOOT_NODES:       $BOOT_NODES"
        ;;
    --name) NAME="--name $2"
        echo "NAME:             $NAME"
        ;;
    --node-key) NODE_KEY="--node-key $2"
        echo "NODE_KEY:         $NODE_KEY"
        ;;
    --rpc-cors) RPC_CORS="--rpc-cors $2"
        echo "RPC_CORS:         $RPC_CORS"
        ;;
    --rpc-methods) RPC_METHODS="--rpc-methods $2"
        echo "RPC_METHODS:      $RPC_METHODS"
        ;;
    --state-pruning) STATE_PRUNING="--state-pruning $2"
        echo "STATE_PRUNING:    $STATE_PRUNING"
        ;;
    --suri) SURI="$2"
        echo "SURI:             $SURI"
        ;;
    --validator) VALIDATOR="--validator"
        echo "VALIDATOR:        $VALIDATOR"
        ;;
  esac
  shift
done

if [ -z "$(ls -A ./chains/testnet/keystore)" ]
then
    if [ ! -z "$SURI" ]
    then
        ./peerplays key insert --base-path ./ --chain testnet --suri "$SURI" --key-type gran --scheme Ed25519
        ./peerplays key insert --base-path ./ --chain testnet --suri "$SURI" --key-type babe --scheme Sr25519
    fi
fi

echo "./peerplays --base-path ./ --chain testnet $BOOT_NODES $NAME $NODE_KEY --port 30333 --prometheus-external $RPC_CORS --rpc-external $RPC_METHODS $STATE_PRUNING --telemetry-url \"ws://10.10.10.71:8001/submit 0\" $VALIDATOR"
./peerplays --base-path ./ --chain testnet $BOOT_NODES $NAME $NODE_KEY --port 30333 --prometheus-external $RPC_CORS --rpc-external $RPC_METHODS $STATE_PRUNING --telemetry-url "ws://10.10.10.71:8001/submit 0" $VALIDATOR
